//  [Section] Comments

// Comments are parts of the code taht gets ignored by the language.
// Coments are meant to be describe the writter code.

// There are two types of comments:
// - Single line c omment (ctrl + /) denoted by two slashes (//)

/*
  -Multiline comments
  -denoted by / ** /
  -ctrl + shift + /

*/

// [SECTION] Statements and Syntax
// Statements
// In programming language, statements are instructions that well tell the computer to perform.
// JS statements usually ends with semicolon (;)
// Semicolons are not required in JS.
// it use to help us train or locate where a statement ends

// Syntax
// In prograaming, it is the set of rules that describes how statements must be consturcted.

console.log('Hello world');

// [SECTION] Variables
//  It is used to contain data.

// Declaring a variable
// tells our devices that a variable name is created and is ready to store data.

// Syntax : let/const variableName;

// NOTE: let is a keyword that is usauslly used in declaring a variable.

let myVariable;
console.log(myVariable); //usuful for printing values of a variable

// Initialize a value
// Storing the initial value of a variable.
// NOTE: Assignment operator (=)

myVariable = 'Hello';

// NOTE: Reassignment a variable value
// changing the initial value to a new value.
myVariable = 'Hello Word';
console.log(myVariable);

// NOTE: const
//  const keywrod is used to declare and initialized a constant variable.
//A "constant" variable does

// const PI;
// PI = 3.1416;
// console.log(PI); //error: initial value is missing for const.

//Declaration with Initialization
// a variable is givin it's initial/starting value upon declaration.
// Syntax: let/const variablename = value;

let productName = 'Desktop Computer';
console.log(productName);

let productPrice = 18999;
// let productPrice = 20000; error : variable has been declared
productPrice = 2000;
console.log(productPrice);

//NOTE: const keyword
const PI = 3.1416;
// PI = 3.14; error: assignmenet to constant varible.
console.log(PI);

/*
  NOTE: Guide in writint variables.
        1. Use the "let" keywrod if the variable will contaion different values.
        or can change over time.
        Naming convetions: camelCasing. variable name should starts with
        lowercase characters and camecasing is use for multple words.
        2. Use the "const" keyword if the variable does not changed.
        Naming conventions "const" keyword should be followeed by
        all CAPITAL VARIABLE NAME.(single value variable).
        3. Varialbe names shoule be indiavative (or descriptive) of
        the value being stored to avoid confusion.
        4. Variable names are case sensitive

*/

// NOTE: Multiple variable declaration

let productCode = 'DC017',
  productBrand = 'Dell';

console.log(productCode, productBrand);

//NOTE: Using a reserve keyword
// const let = "hello";
// console.log(let); error: lexically bound is disabllowed

// [SECTION] Data Types

// In JavaScript, there are six types of data

//NOTE: Strings - are series of characteers that create a world, a phase, a sentece or anyting
//  related to creating text.
// Strings in JavaScript can be written using a single quote(') or double quote("")

let country = 'Philippines';
let province = 'Metro Manila';

//NOTE: Concatenating Strings
// Multiple strings values that can be combined to  create a single String
// using the "+" symbol.

// I live in metro manila, philippines
let greetings = 'I live in ' + province + ', ' + country;
console.log(greetings);

// NOTE: Escape Characters
// (\) in string combination with other characters can produce different reuslts
// (\n) refers to creating new line.
let mailAddress = 'Quezon City \n Philippines';
console.log(mailAddress);

//NOTE: expected output: Jonh's employees went home early.
let message = "John's employees went home early.";
console.log(message);

//using the escape character (/)
message = "Jane's employees went home early.";
console.log(message);

// NOTE: Numbers
//intergers/whole numbers
let headcount = 26;
console.log(headcount);

// Decimal numbers/fractions
let grade = 98.7;
console.log(grade);

//exponentioal notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine text and stirngs
console.log("John's grade last quarter is " + grade);

// NOTE: Boolean
// boolean values are normally used to store values relating to the start of
// certain things.
// valeu of boolean = true or false

let isMarried = false;
let isGoodConduct = true;

console.log('isMarried: ' + isMarried);
console.log('isGoodConduct: ' + isGoodConduct);

// NOTE: Arrays - are special kind of data types that can used to store
//  multiple related values
//  Syntax: let/const arrayName = [elementA, elemendB ... elementNth];

const grades = [98.7, 92.1, 90.2, 94.6];
// constant variable cannot be reassigned.
//  grades = 100; is not allowerd
//  Changing the elements of an array or changin the properties of an object is allowed in constant variable.
grades[0] = 100;
console.log(grades);

//This will work but not recommended.
let details = ['John', 'Smith', 32, true];
console.log(details);

// NOTE: Objects - are another special kind of data type that is
//  used to mimi real world objects or items.
//  used to create complex data that contains information relevant to each other

/*
  Syntax: let/const objectName= {
    propertyA: valueA
    propertyB: valueB
  }

*/

let person = {
  fullName: 'Juan Dela Cruz',
  age: 35,
  isMarried: false,
  contact: ['+63912 345 6789', '8123 456'],
  address: {
    houseNumber: '345',
    city: 'Manila',
  },
};

console.log(person);

//  They're are also usefull for creating abstract objects

let myGrades = {
  firstGrading: 98.7,
  secondGrading: 92.1,
  thirdGrading: 90.2,
  fourthGrading: 94.6,
};

console.log(myGrades);

// NOTE: typeof operato is used to determined the type of data or the value of a variable

console.log(typeof myGrades); //result is object

// array is special typ of object with methods.
console.log(typeof grades);

// NOTE: null - indicates the absence of a value.
let spouse = null;
console.log(spouse);

// undefined - indicates that a variables has not been given a value yet.
